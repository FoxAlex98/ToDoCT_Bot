import sqlite3

from utils.keyboard import *
from utils.phrases import *
from utils.querybuilder import *
from settings import *
from telegram.ext import ConversationHandler
from telegram import Update
from telegram import ReplyKeyboardMarkup
from datetime import datetime, timedelta
import pytz


TASK = range(1)

def start(update, context):
    user = update.message.from_user
    chat_id = update.message.chat_id
    try:
        dbconn = sqlite3.connect('data/ToDo_DB.db')
        dbconn.execute(insert_user_query(chat_id,user['first_name'],user['last_name'],user['username']))
        dbconn.commit()
    except Exception as error:
        print(error)
        context.bot.send_message(chat_id = chat_id, text = "ERRORE INSERIMENTO UTENTE")
    context.bot.send_message(chat_id = chat_id, text = start_phrase(user['first_name']), reply_markup = ReplyKeyboardMarkup(getkeyboard()))

def add(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id = chat_id, text = "Inserisci la nota", reply_markup = ReplyKeyboardMarkup(getkeyboard()))
    return TASK

def insert_task(update, context):
    chat_id = update.message.chat_id
    tz = pytz.timezone('Europe/Rome')
    tr = datetime.now(tz)
    try:
        dbconn = sqlite3.connect('data/ToDo_DB.db')
        dbconn.execute(insert_task_query(chat_id,update.message.text,"nullo",tr))
        dbconn.commit()
    except Exception as error:
        print(error)
        context.bot.send_message(chat_id = chat_id, text = "ERRORE INSERIMENTO TASK")
        return ConversationHandler.END
    context.bot.send_message(chat_id = chat_id, text = "nota inserita", reply_markup = ReplyKeyboardMarkup(getkeyboard()))
    return ConversationHandler.END

def cancel(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id = chat_id, text = "Azione annullata", reply_markup = ReplyKeyboardMarkup(getkeyboard()))
    return ConversationHandler.END

def add_from_line(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id = chat_id, text = "anche questa è in sviluppo")

def remove(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id = chat_id, text = "demo", reply_markup = ReplyKeyboardMarkup(getkeyboard()))

def to_do_list(update, context):
    chat_id = update.message.chat_id
    try:
        dbconn = sqlite3.connect('data/ToDo_DB.db')
        tasks = dbconn.execute(get_list(chat_id)).fetchall()
        response = get_list_phrase(tasks)
    except Exception as error:
        print(error)
    context.bot.send_message(chat_id = chat_id, text = response, reply_markup = ReplyKeyboardMarkup(getkeyboard()))

def help_me(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id = chat_id, text = help_phrase, reply_markup = ReplyKeyboardMarkup(getkeyboard()))

def info_point(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id = chat_id, text = info_phrase, reply_markup = ReplyKeyboardMarkup(getkeyboard()))

def donates(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id = chat_id, text = donates_phrase, reply_markup = ReplyKeyboardMarkup(getkeyboard()))

def viewusers(update, context):
    chat_id = update.message.chat_id
    if(chat_id == ADMIN):
        try:
            dbconn = sqlite3.connect('data/ToDo_DB.db')
            cursor = dbconn.cursor()
            cursor.execute(view_users_query())
            users = cursor.fetchall()
            response = "utenti\n"
            for row in users:
                response += "Id " + str(row[0]) + "\n "
                response += "Nome " + row[1] + "\n"
                response += "Cognome " + row[2] + "\n"
                response += "Username " + row[3] + "\n"
                response += "------------ \n"
            context.bot.send_message(chat_id = chat_id, text = response)
        except Exception as error:
            print(error)
            context.bot.send_message(chat_id = chat_id, text = "ERRORE SELECT UTENTE")
    else:
        context.bot.send_message(chat_id = chat_id, text = "tu non sei il boss", reply_markup = ReplyKeyboardMarkup(getkeyboard()))
