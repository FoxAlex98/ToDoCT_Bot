def start_phrase(name):
    return "Ciao " + name + ", questo bot ti permetterà di salvare " + \
        "delle note in un tuo ToDoList\nPuoi usare questo bot anche all'interno dei gruppi"

def get_list_phrase(tasks):
    task_list = "Questi sono i tuoi Task\n"
    for row in tasks:
        task_list += "- " + row[0] + "\n"
        if(row[1]):
            task_list += " > " + row[1] + "\n"
        task_list += "----------\n"
    return task_list

        

help_phrase = "queste sono le operazioni che puoi fare"

info_phrase = "questo bot serve per fare liste di cose da fare"

donates_phrase = "se vuoi donare qualcosa, hai voglia"