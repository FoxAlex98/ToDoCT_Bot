import sqlite3
import os
import sys

from settings import *
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler
from functions import *


updater = Updater(token=TOKEN, use_context=True)
dispatcher = updater.dispatcher



def echo(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)

#Command
dispatcher.add_handler(CommandHandler('start',start))
dispatcher.add_handler(CommandHandler('add',add_from_line))
dispatcher.add_handler(CommandHandler('remove',remove))
dispatcher.add_handler(CommandHandler('list',to_do_list))
dispatcher.add_handler(CommandHandler('help',help_me))
dispatcher.add_handler(CommandHandler('info',info_point))
dispatcher.add_handler(CommandHandler('donates',donates))

#Message
#dispatcher.add_handler(MessageHandler(Filters.regex('Aggiungi'),add))
dispatcher.add_handler(MessageHandler(Filters.regex('Rimuovi'),remove))
dispatcher.add_handler(MessageHandler(Filters.regex('Lista'),to_do_list))
dispatcher.add_handler(MessageHandler(Filters.regex('Aiuto'),help_me))
dispatcher.add_handler(MessageHandler(Filters.regex('Info'),info_point))
dispatcher.add_handler(MessageHandler(Filters.regex('Dona'),donates))
dispatcher.add_handler(MessageHandler(Filters.regex('Utenti'),viewusers))
#dispatcher.add_handler(MessageHandler(Filters.text,echo))

#Conversation

#TODO: da completare
adder = ConversationHandler(
    entry_points=[MessageHandler(Filters.regex('Aggiungi'),add)],
    states={
        TASK: [MessageHandler(Filters.text, insert_task)],
    },
    fallbacks=[CommandHandler('annulla',cancel)]
)

dispatcher.add_handler(adder)

updater.start_polling()