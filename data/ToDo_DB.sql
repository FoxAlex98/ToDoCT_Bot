--Users table

CREATE TABLE IF NOT EXISTS "Users" (
    "chat_id" INTEGER PRIMARY KEY,
    "name"    VARCHAR(255) NOT NULL,
    "surname" VARCHAR(255) NOT NULL,
    "nickname" VARCHAR(255)
);

--Tasks table

CREATE TABLE IF NOT EXISTS "Tasks"(
    "id_task" INTEGER PRIMARY KEY AUTOINCREMENT,
    "chat_id" INTEGER NOT NULL,
    "title" text NOT NULL,
    "description" text,
     date  DATE,
     FOREIGN KEY("chat_id") REFERENCES "Users"("chat_id"),
     UNIQUE("id_task","chat_id")
);